﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Fílek Soft")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyDescription("Server Tools for CoD:MW")]
[assembly: AssemblyProduct("Server Tools for CoD:MW")]
[assembly: AssemblyCopyright("Copyright © Fílek 2011")]
[assembly: AssemblyTitle("Server Tools for CoD:MW")]
[assembly: ComVisible(false)]
[assembly: Guid("7a7a3e7d-e46f-4c4c-b5bb-b41367732d9b")]
[assembly: AssemblyFileVersion("2.0.0.0")]
[assembly: AssemblyVersion("2.0.0.0")]
