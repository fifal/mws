﻿// Decompiled with JetBrains decompiler
// Type: MWS.Program
// Assembly: MWS, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BDEF8AEC-8A94-4FB3-8152-B098A3F52774
// Assembly location: C:\Users\Fifal\Desktop\MWS.exe

using System;
using System.Windows.Forms;

namespace MWS
{
  internal static class Program
  {
    public static Form1 frm;
    public static Seznam szn;
    public static string ip;
    public static int port;
    public static string rcon = "1234";
    public static string nick;
    public static int id;
    public static string score;
    public static string cesta;
    public static string guid;
    public static banned ban;

    [STAThread]
    private static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Program.frm = new Form1();
      Program.ban = new banned();
      Program.szn = new Seznam();
      Application.Run((Form) Program.frm);
    }
  }
}
