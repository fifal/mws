﻿// Decompiled with JetBrains decompiler
// Type: MWS.RCONfrm
// Assembly: MWS, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BDEF8AEC-8A94-4FB3-8152-B098A3F52774
// Assembly location: C:\Users\Fifal\Desktop\MWS.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace MWS
{
  public class RCONfrm : Form
  {
    private IContainer components;
    private Label status_lbl;
    private RichTextBox richTextBox1;
    private GroupBox groupBox1;
    private Button info_btn;
    private Button status_btn;
    private Button save_btn;
    private Button button1;
    private TextBox textBox1;
    private Button button2;
    private GroupBox groupBox3;
    private Button button7;
    private ListBox listBox1;
    private Button button4;
    private Button button3;
    private Button button5;
    private Button banned_btn;
    private Button button6;
    public string[] mapy = new string[21]
    {
      "mp_backlot",
      "mp_bloc",
      "mp_bog",
      "mp_broadcast",
      "mp_carentan",
      "mp_cargoship",
      "mp_citystreets",
      "mp_convoy",
      "mp_countdown",
      "mp_crash",
      "mp_crash_snow",
      "mp_creek",
      "mp_crossfire",
      "mp_farm",
      "mp_killhouse",
      "mp_overgrown",
      "mp_pipeline",
      "mp_shipment",
      "mp_showdown",
      "mp_strike",
      "mp_vacant"
    };
    private Funkce fce = new Funkce();

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (RCONfrm));
      this.status_lbl = new Label();
      this.richTextBox1 = new RichTextBox();
      this.groupBox1 = new GroupBox();
      this.banned_btn = new Button();
      this.button5 = new Button();
      this.button4 = new Button();
      this.button3 = new Button();
      this.groupBox3 = new GroupBox();
      this.button7 = new Button();
      this.listBox1 = new ListBox();
      this.button1 = new Button();
      this.status_btn = new Button();
      this.textBox1 = new TextBox();
      this.info_btn = new Button();
      this.save_btn = new Button();
      this.button2 = new Button();
      this.button6 = new Button();
      this.groupBox1.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.SuspendLayout();
      this.status_lbl.AutoSize = true;
      this.status_lbl.Location = new Point(12, 9);
      this.status_lbl.Name = "status_lbl";
      this.status_lbl.Size = new Size(31, 13);
      this.status_lbl.TabIndex = 0;
      this.status_lbl.Text = "Log: ";
      this.richTextBox1.BorderStyle = BorderStyle.FixedSingle;
      this.richTextBox1.Location = new Point(12, 25);
      this.richTextBox1.Name = "richTextBox1";
      this.richTextBox1.Size = new Size(395, 410);
      this.richTextBox1.TabIndex = 1;
      this.richTextBox1.Text = "";
      this.richTextBox1.TextChanged += new EventHandler(this.richTextBox1_TextChanged);
      this.groupBox1.Controls.Add((Control) this.button6);
      this.groupBox1.Controls.Add((Control) this.banned_btn);
      this.groupBox1.Controls.Add((Control) this.button5);
      this.groupBox1.Controls.Add((Control) this.button4);
      this.groupBox1.Controls.Add((Control) this.button3);
      this.groupBox1.Controls.Add((Control) this.groupBox3);
      this.groupBox1.Controls.Add((Control) this.button1);
      this.groupBox1.Controls.Add((Control) this.status_btn);
      this.groupBox1.Controls.Add((Control) this.textBox1);
      this.groupBox1.Controls.Add((Control) this.info_btn);
      this.groupBox1.Location = new Point(413, 12);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new Size(279, 450);
      this.groupBox1.TabIndex = 2;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Příkazy:";
      this.banned_btn.Location = new Point(166, 132);
      this.banned_btn.Name = "banned_btn";
      this.banned_btn.Size = new Size(107, 23);
      this.banned_btn.TabIndex = 16;
      this.banned_btn.Text = "Zabanovaní hráči";
      this.banned_btn.UseVisualStyleBackColor = true;
      this.banned_btn.Click += new EventHandler(this.banned_btn_Click);
      this.button5.Location = new Point(166, 103);
      this.button5.Name = "button5";
      this.button5.Size = new Size(107, 23);
      this.button5.TabIndex = 15;
      this.button5.Text = "Seznam hráčů";
      this.button5.UseVisualStyleBackColor = true;
      this.button5.Click += new EventHandler(this.button5_Click);
      this.button4.Location = new Point(142, 45);
      this.button4.Name = "button4";
      this.button4.Size = new Size(131, 23);
      this.button4.TabIndex = 14;
      this.button4.Text = "map_restart";
      this.button4.UseVisualStyleBackColor = true;
      this.button4.Click += new EventHandler(this.button4_Click);
      this.button3.Location = new Point(6, 45);
      this.button3.Name = "button3";
      this.button3.Size = new Size(131, 23);
      this.button3.TabIndex = 13;
      this.button3.Text = "fast_restart";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new EventHandler(this.button3_Click);
      this.groupBox3.Controls.Add((Control) this.button7);
      this.groupBox3.Controls.Add((Control) this.listBox1);
      this.groupBox3.ForeColor = SystemColors.HotTrack;
      this.groupBox3.Location = new Point(6, 74);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new Size(154, 343);
      this.groupBox3.TabIndex = 12;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Změnit mapu";
      this.button7.ForeColor = SystemColors.InfoText;
      this.button7.Location = new Point(4, 315);
      this.button7.Name = "button7";
      this.button7.Size = new Size(142, 23);
      this.button7.TabIndex = 20;
      this.button7.Text = "Změnit mapu";
      this.button7.UseVisualStyleBackColor = true;
      this.button7.Click += new EventHandler(this.button7_Click);
      this.listBox1.FormattingEnabled = true;
      this.listBox1.Location = new Point(4, 19);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new Size(142, 290);
      this.listBox1.TabIndex = 19;
      this.button1.Location = new Point(190, 420);
      this.button1.Name = "button1";
      this.button1.Size = new Size(83, 23);
      this.button1.TabIndex = 1;
      this.button1.Text = "Poslat příkaz";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click_1);
      this.status_btn.Location = new Point(142, 19);
      this.status_btn.Name = "status_btn";
      this.status_btn.Size = new Size(131, 23);
      this.status_btn.TabIndex = 4;
      this.status_btn.Text = "status";
      this.status_btn.UseVisualStyleBackColor = true;
      this.status_btn.Click += new EventHandler(this.button2_Click);
      this.textBox1.Location = new Point(10, 422);
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new Size(172, 20);
      this.textBox1.TabIndex = 0;
      this.info_btn.Location = new Point(6, 19);
      this.info_btn.Name = "info_btn";
      this.info_btn.Size = new Size(131, 23);
      this.info_btn.TabIndex = 3;
      this.info_btn.Text = "serverinfo";
      this.info_btn.UseVisualStyleBackColor = true;
      this.info_btn.Click += new EventHandler(this.button1_Click);
      this.save_btn.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 238);
      this.save_btn.Location = new Point(12, 439);
      this.save_btn.Name = "save_btn";
      this.save_btn.Size = new Size(197, 23);
      this.save_btn.TabIndex = 5;
      this.save_btn.Text = "Uložit log";
      this.save_btn.UseVisualStyleBackColor = true;
      this.save_btn.Click += new EventHandler(this.save_btn_Click);
      this.button2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 238);
      this.button2.ForeColor = Color.Red;
      this.button2.Location = new Point(210, 439);
      this.button2.Name = "button2";
      this.button2.Size = new Size(197, 23);
      this.button2.TabIndex = 5;
      this.button2.Text = "RCON QUIT";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click_1);
      this.button6.Location = new Point(166, 74);
      this.button6.Name = "button6";
      this.button6.Size = new Size(105, 23);
      this.button6.TabIndex = 23;
      this.button6.Text = "Zapnout hardcore";
      this.button6.UseVisualStyleBackColor = true;
      this.button6.Click += new EventHandler(this.button6_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(704, 474);
      this.Controls.Add((Control) this.save_btn);
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.groupBox1);
      this.Controls.Add((Control) this.richTextBox1);
      this.Controls.Add((Control) this.status_lbl);
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximumSize = new Size(720, 512);
      this.MinimumSize = new Size(720, 512);
      this.Name = nameof (RCONfrm);
      this.Text = "RCON Control Panel";
      this.Load += new EventHandler(this.RCONfrm_Load);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public RCONfrm() => this.InitializeComponent();

    private void button1_Click(object sender, EventArgs e) => this.richTextBox1.Text = this.fce.odeslatPrikaz("serverinfo");

    private void button2_Click(object sender, EventArgs e) => this.richTextBox1.Text = this.fce.odeslatPrikaz("status");

    private void richTextBox1_TextChanged(object sender, EventArgs e) => this.richTextBox1.Text = this.richTextBox1.Text.Replace("????print", "");

    private void save_btn_Click(object sender, EventArgs e)
    {
      StreamWriter streamWriter = new StreamWriter(Application.StartupPath + "/log.txt");
      streamWriter.Write(this.richTextBox1.Text);
      streamWriter.Close();
    }

    private void button1_Click_1(object sender, EventArgs e)
    {
      try
      {
        this.richTextBox1.Text = this.fce.odeslatPrikaz(this.textBox1.Text);
        this.richTextBox1.Text += "Úspěšně odesláno!";
      }
      catch
      {
        this.richTextBox1.Text += "Chyba!";
      }
    }

    private void button2_Click_1(object sender, EventArgs e)
    {
      this.richTextBox1.Text = this.fce.odeslatPrikaz("quit");
      this.richTextBox1.Text += "Server byl ukončen!";
    }

    private void RCONfrm_Load(object sender, EventArgs e)
    {
      foreach (object obj in this.mapy)
        this.listBox1.Items.Add(obj);
    }

    private void button7_Click(object sender, EventArgs e) => this.richTextBox1.Text = this.fce.odeslatPrikaz("map " + this.listBox1.SelectedItem);

    private void button3_Click(object sender, EventArgs e) => this.richTextBox1.Text = this.fce.odeslatPrikaz("fast_restart");

    private void button4_Click(object sender, EventArgs e) => this.richTextBox1.Text = this.fce.odeslatPrikaz("map_restart");

    private void button5_Click(object sender, EventArgs e)
    {
      int num1 = 0;
      if (num1 == 0)
        Program.szn.Show();
      else
        Program.szn.Focus();
      int num2 = num1 + 1;
    }

    private void banned_btn_Click(object sender, EventArgs e)
    {
      int num1 = 0;
      if (num1 == 0)
        Program.ban.Show();
      else
        Program.ban.Focus();
      int num2 = num1 + 1;
    }

    private void button6_Click(object sender, EventArgs e)
    {
      this.fce.odeslatPrikaz("scr_hardcore 1");
      this.fce.odeslatPrikaz("scr_dm_playerrespawndelay 1");
      this.fce.odeslatPrikaz("scr_dm_waverespawndelay 1");
      this.fce.odeslatPrikaz("fast_restart");
    }
  }
}
