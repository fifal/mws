﻿// Decompiled with JetBrains decompiler
// Type: MWS.Seznam
// Assembly: MWS, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BDEF8AEC-8A94-4FB3-8152-B098A3F52774
// Assembly location: C:\Users\Fifal\Desktop\MWS.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace MWS
{
  public class Seznam : Form
  {
    private IContainer components;
    private Button button1;
    private DataGridViewTextBoxColumn ID;
    private DataGridViewTextBoxColumn Ping;
    private DataGridViewTextBoxColumn GUID;
    private DataGridViewTextBoxColumn Nick;
    public DataGridView seznamGrid;
    private Button button2;
    private DataGridViewTextBoxColumn score;
    private Funkce fce = new Funkce();

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (Seznam));
      this.seznamGrid = new DataGridView();
      this.ID = new DataGridViewTextBoxColumn();
      this.Ping = new DataGridViewTextBoxColumn();
      this.GUID = new DataGridViewTextBoxColumn();
      this.Nick = new DataGridViewTextBoxColumn();
      this.score = new DataGridViewTextBoxColumn();
      this.button1 = new Button();
      this.button2 = new Button();
      ((ISupportInitialize) this.seznamGrid).BeginInit();
      this.SuspendLayout();
      this.seznamGrid.AllowUserToAddRows = false;
      this.seznamGrid.AllowUserToDeleteRows = false;
      this.seznamGrid.AllowUserToResizeColumns = false;
      this.seznamGrid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.seznamGrid.Columns.AddRange((DataGridViewColumn) this.ID, (DataGridViewColumn) this.Ping, (DataGridViewColumn) this.GUID, (DataGridViewColumn) this.Nick, (DataGridViewColumn) this.score);
      this.seznamGrid.Location = new Point(12, 12);
      this.seznamGrid.Name = "seznamGrid";
      this.seznamGrid.ReadOnly = true;
      this.seznamGrid.Size = new Size(554, 239);
      this.seznamGrid.TabIndex = 0;
      this.seznamGrid.CellDoubleClick += new DataGridViewCellEventHandler(this.seznamGrid_CellDoubleClick);
      this.ID.FillWeight = 30f;
      this.ID.HeaderText = "ID";
      this.ID.Name = "ID";
      this.ID.ReadOnly = true;
      this.ID.Width = 30;
      this.Ping.FillWeight = 35f;
      this.Ping.HeaderText = "Ping";
      this.Ping.Name = "Ping";
      this.Ping.ReadOnly = true;
      this.Ping.Width = 35;
      this.GUID.FillWeight = 200f;
      this.GUID.HeaderText = "GUID";
      this.GUID.Name = "GUID";
      this.GUID.ReadOnly = true;
      this.GUID.Width = 200;
      this.Nick.HeaderText = "Nick";
      this.Nick.Name = "Nick";
      this.Nick.ReadOnly = true;
      this.Nick.Width = 245;
      this.score.HeaderText = "score";
      this.score.Name = "score";
      this.score.ReadOnly = true;
      this.score.Visible = false;
      this.button1.Location = new Point(12, 257);
      this.button1.Name = "button1";
      this.button1.Size = new Size(418, 23);
      this.button1.TabIndex = 1;
      this.button1.Text = "Načíst";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.button2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 238);
      this.button2.ForeColor = Color.Red;
      this.button2.Location = new Point(436, 257);
      this.button2.Name = "button2";
      this.button2.Size = new Size(130, 23);
      this.button2.TabIndex = 2;
      this.button2.Text = "Konec";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(578, 289);
      this.ControlBox = false;
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.button1);
      this.Controls.Add((Control) this.seznamGrid);
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.Name = nameof (Seznam);
      this.Text = "Seznam hráčů";
      ((ISupportInitialize) this.seznamGrid).EndInit();
      this.ResumeLayout(false);
    }

    public Seznam() => this.InitializeComponent();

    private void button1_Click(object sender, EventArgs e) => this.fce.nacistSeznamHracu();

    private void seznamGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
    {
      Program.nick = this.seznamGrid.Rows[e.RowIndex].Cells[3].Value.ToString();
      Program.id = Convert.ToInt32(this.seznamGrid.Rows[e.RowIndex].Cells[0].Value);
      Program.score = this.seznamGrid.Rows[e.RowIndex].Cells[4].Value.ToString();
      int num = (int) new dialog().ShowDialog();
    }

    private void timer1_Tick_1(object sender, EventArgs e) => this.fce.nacistSeznamHracu();

    private void button2_Click(object sender, EventArgs e) => Program.szn.Hide();
  }
}
