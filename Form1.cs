﻿// Decompiled with JetBrains decompiler
// Type: MWS.Form1
// Assembly: MWS, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BDEF8AEC-8A94-4FB3-8152-B098A3F52774
// Assembly location: C:\Users\Fifal\Desktop\MWS.exe

using INI;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace MWS
{
  public class Form1 : Form
  {
    private IContainer components;
    private TextBox iw3mp_txt;
    private Label label1;
    private Button button1;
    private GroupBox groupBox1;
    private Label label2;
    private ComboBox modif_combo;
    private TextBox game_text;
    private Label label3;
    private ComboBox dedik_combo;
    private Label label5;
    private TextBox cfg_text;
    private CheckBox checkBox1;
    private Label ip_lbl;
    private TextBox ip_text;
    private Button start_btn;
    private RichTextBox config_rtf;
    private Button button3;
    private Button button4;
    private CheckBox checkBox2;
    private Button connect_btn;
    private Label label6;
    private GroupBox groupBox2;
    private Label label7;
    private GroupBox groupBox3;
    private ListBox listBox1;
    private Button button6;
    private ComboBox rot_combo;
    private Button button7;
    private Button startBezSrv_btn;
    private LinkLabel linkLabel1;
    private Button rcon_btn;
    private Label label4;
    private TextBox rcon_txt;
    private Button button2;
    private Button button5;
    private TextBox server_txt;
    private Label label8;
    private LinkLabel linkLabel2;
    private Label label9;
    public string modifikace = "";
    public string[] mapy = new string[21]
    {
      "mp_backlot",
      "mp_bloc",
      "mp_bog",
      "mp_broadcast",
      "mp_carentan",
      "mp_cargoship",
      "mp_citystreets",
      "mp_convoy",
      "mp_countdown",
      "mp_crash",
      "mp_crash_snow",
      "mp_creek",
      "mp_crossfire",
      "mp_farm",
      "mp_killhouse",
      "mp_overgrown",
      "mp_pipeline",
      "mp_shipment",
      "mp_showdown",
      "mp_strike",
      "mp_vacant"
    };
    public string config = "";
    private Funkce funkce = new Funkce();

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (Form1));
      this.iw3mp_txt = new TextBox();
      this.label1 = new Label();
      this.button1 = new Button();
      this.groupBox1 = new GroupBox();
      this.server_txt = new TextBox();
      this.label8 = new Label();
      this.rcon_txt = new TextBox();
      this.label4 = new Label();
      this.checkBox2 = new CheckBox();
      this.button4 = new Button();
      this.button3 = new Button();
      this.dedik_combo = new ComboBox();
      this.label5 = new Label();
      this.cfg_text = new TextBox();
      this.checkBox1 = new CheckBox();
      this.game_text = new TextBox();
      this.label3 = new Label();
      this.label2 = new Label();
      this.modif_combo = new ComboBox();
      this.ip_lbl = new Label();
      this.ip_text = new TextBox();
      this.start_btn = new Button();
      this.config_rtf = new RichTextBox();
      this.connect_btn = new Button();
      this.label6 = new Label();
      this.groupBox2 = new GroupBox();
      this.label7 = new Label();
      this.groupBox3 = new GroupBox();
      this.button7 = new Button();
      this.listBox1 = new ListBox();
      this.button6 = new Button();
      this.rot_combo = new ComboBox();
      this.startBezSrv_btn = new Button();
      this.linkLabel1 = new LinkLabel();
      this.rcon_btn = new Button();
      this.button2 = new Button();
      this.button5 = new Button();
      this.linkLabel2 = new LinkLabel();
      this.label9 = new Label();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.SuspendLayout();
      this.iw3mp_txt.Location = new Point(55, 12);
      this.iw3mp_txt.Name = "iw3mp_txt";
      this.iw3mp_txt.ReadOnly = true;
      this.iw3mp_txt.Size = new Size(362, 20);
      this.iw3mp_txt.TabIndex = 0;
      this.iw3mp_txt.TextChanged += new EventHandler(this.iw3mp_txt_TextChanged);
      this.label1.AutoSize = true;
      this.label1.BackColor = Color.Transparent;
      this.label1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.label1.ForeColor = SystemColors.Desktop;
      this.label1.Location = new Point(9, 15);
      this.label1.Name = "label1";
      this.label1.Size = new Size(40, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "iw3mp:";
      this.button1.Location = new Point(423, 12);
      this.button1.Name = "button1";
      this.button1.Size = new Size(112, 23);
      this.button1.TabIndex = 2;
      this.button1.Text = "Vybrat";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.groupBox1.BackColor = Color.Transparent;
      this.groupBox1.Controls.Add((Control) this.server_txt);
      this.groupBox1.Controls.Add((Control) this.label8);
      this.groupBox1.Controls.Add((Control) this.rcon_txt);
      this.groupBox1.Controls.Add((Control) this.label4);
      this.groupBox1.Controls.Add((Control) this.checkBox2);
      this.groupBox1.Controls.Add((Control) this.button4);
      this.groupBox1.Controls.Add((Control) this.button3);
      this.groupBox1.Controls.Add((Control) this.dedik_combo);
      this.groupBox1.Controls.Add((Control) this.label5);
      this.groupBox1.Controls.Add((Control) this.cfg_text);
      this.groupBox1.Controls.Add((Control) this.checkBox1);
      this.groupBox1.Controls.Add((Control) this.game_text);
      this.groupBox1.Controls.Add((Control) this.label3);
      this.groupBox1.Controls.Add((Control) this.label2);
      this.groupBox1.Controls.Add((Control) this.modif_combo);
      this.groupBox1.FlatStyle = FlatStyle.Flat;
      this.groupBox1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.groupBox1.ForeColor = SystemColors.HotTrack;
      this.groupBox1.Location = new Point(6, 38);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new Size(529, 122);
      this.groupBox1.TabIndex = 3;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Nastavení";
      this.server_txt.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.server_txt.ForeColor = SystemColors.ActiveCaptionText;
      this.server_txt.Location = new Point(337, 67);
      this.server_txt.Name = "server_txt";
      this.server_txt.Size = new Size(176, 20);
      this.server_txt.TabIndex = 20;
      this.server_txt.Text = "^6M^7WS by Fílek";
      this.label8.AutoSize = true;
      this.label8.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.label8.ForeColor = SystemColors.ActiveCaptionText;
      this.label8.Location = new Point(256, 70);
      this.label8.Name = "label8";
      this.label8.Size = new Size(79, 13);
      this.label8.TabIndex = 19;
      this.label8.Text = "Název serveru:";
      this.rcon_txt.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.rcon_txt.Location = new Point(80, 67);
      this.rcon_txt.Name = "rcon_txt";
      this.rcon_txt.Size = new Size(156, 20);
      this.rcon_txt.TabIndex = 18;
      this.rcon_txt.Text = "1234";
      this.rcon_txt.TextChanged += new EventHandler(this.rcon_txt_TextChanged);
      this.label4.AutoSize = true;
      this.label4.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.label4.ForeColor = SystemColors.Desktop;
      this.label4.Location = new Point(33, 70);
      this.label4.Name = "label4";
      this.label4.Size = new Size(41, 13);
      this.label4.TabIndex = 17;
      this.label4.Text = "RCON:";
      this.checkBox2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.checkBox2.Location = new Point(8, 20);
      this.checkBox2.Name = "checkBox2";
      this.checkBox2.Size = new Size(15, 18);
      this.checkBox2.TabIndex = 16;
      this.checkBox2.Text = "checkBox2";
      this.checkBox2.UseVisualStyleBackColor = true;
      this.checkBox2.CheckedChanged += new EventHandler(this.checkBox2_CheckedChanged);
      this.button4.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.button4.ForeColor = SystemColors.ActiveCaptionText;
      this.button4.Location = new Point(268, 90);
      this.button4.Name = "button4";
      this.button4.Size = new Size(251, 23);
      this.button4.TabIndex = 15;
      this.button4.Text = "Nový CFG";
      this.button4.UseVisualStyleBackColor = true;
      this.button4.Click += new EventHandler(this.button4_Click);
      this.button3.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.button3.ForeColor = SystemColors.ActiveCaptionText;
      this.button3.Location = new Point(6, 90);
      this.button3.Name = "button3";
      this.button3.Size = new Size(251, 23);
      this.button3.TabIndex = 13;
      this.button3.Text = "Uložit změny";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new EventHandler(this.button3_Click);
      this.dedik_combo.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.dedik_combo.FormattingEnabled = true;
      this.dedik_combo.Items.AddRange(new object[3]
      {
        (object) "Ne",
        (object) "Lan",
        (object) "Internet"
      });
      this.dedik_combo.Location = new Point(80, 43);
      this.dedik_combo.Name = "dedik_combo";
      this.dedik_combo.Size = new Size(156, 21);
      this.dedik_combo.TabIndex = 12;
      this.label5.AutoSize = true;
      this.label5.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.label5.ForeColor = SystemColors.Desktop;
      this.label5.Location = new Point(7, 46);
      this.label5.Name = "label5";
      this.label5.Size = new Size(67, 13);
      this.label5.TabIndex = 11;
      this.label5.Text = "Dedikovaný:";
      this.cfg_text.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.cfg_text.ForeColor = SystemColors.ActiveCaptionText;
      this.cfg_text.Location = new Point(337, 43);
      this.cfg_text.Name = "cfg_text";
      this.cfg_text.ReadOnly = true;
      this.cfg_text.Size = new Size(176, 20);
      this.cfg_text.TabIndex = 10;
      this.checkBox1.AutoSize = true;
      this.checkBox1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.checkBox1.ForeColor = SystemColors.ActiveCaptionText;
      this.checkBox1.Location = new Point(242, 45);
      this.checkBox1.Name = "checkBox1";
      this.checkBox1.Size = new Size(93, 17);
      this.checkBox1.TabIndex = 9;
      this.checkBox1.Text = "Načíst config:";
      this.checkBox1.UseVisualStyleBackColor = true;
      this.checkBox1.CheckedChanged += new EventHandler(this.checkBox1_CheckedChanged);
      this.game_text.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.game_text.ForeColor = SystemColors.ActiveCaptionText;
      this.game_text.Location = new Point(316, 19);
      this.game_text.Name = "game_text";
      this.game_text.Size = new Size(197, 20);
      this.game_text.TabIndex = 6;
      this.label3.AutoSize = true;
      this.label3.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.label3.ForeColor = SystemColors.ActiveCaptionText;
      this.label3.Location = new Point(265, 22);
      this.label3.Name = "label3";
      this.label3.Size = new Size(45, 13);
      this.label3.TabIndex = 5;
      this.label3.Text = "Typ hry:";
      this.label2.AutoSize = true;
      this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.label2.ForeColor = SystemColors.Desktop;
      this.label2.Location = new Point(23, 22);
      this.label2.Name = "label2";
      this.label2.Size = new Size(62, 13);
      this.label2.TabIndex = 4;
      this.label2.Text = "Modifikace:";
      this.modif_combo.Enabled = false;
      this.modif_combo.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.modif_combo.FormattingEnabled = true;
      this.modif_combo.Location = new Point(91, 19);
      this.modif_combo.Name = "modif_combo";
      this.modif_combo.Size = new Size(145, 21);
      this.modif_combo.TabIndex = 0;
      this.ip_lbl.AutoSize = true;
      this.ip_lbl.BackColor = Color.Transparent;
      this.ip_lbl.ForeColor = SystemColors.Desktop;
      this.ip_lbl.Location = new Point(133, 423);
      this.ip_lbl.Name = "ip_lbl";
      this.ip_lbl.Size = new Size(163, 13);
      this.ip_lbl.TabIndex = 4;
      this.ip_lbl.Text = "Vaše IP (pro připojení k serveru):";
      this.ip_text.Location = new Point(299, 420);
      this.ip_text.Name = "ip_text";
      this.ip_text.ReadOnly = true;
      this.ip_text.Size = new Size(136, 20);
      this.ip_text.TabIndex = 5;
      this.start_btn.Enabled = false;
      this.start_btn.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Regular, GraphicsUnit.Point, (byte) 238);
      this.start_btn.ForeColor = Color.Red;
      this.start_btn.Location = new Point(6, 377);
      this.start_btn.Name = "start_btn";
      this.start_btn.Size = new Size(529, 37);
      this.start_btn.TabIndex = 6;
      this.start_btn.Text = "Spustit server!";
      this.start_btn.UseVisualStyleBackColor = true;
      this.start_btn.Click += new EventHandler(this.button2_Click);
      this.config_rtf.Location = new Point(6, 166);
      this.config_rtf.Name = "config_rtf";
      this.config_rtf.Size = new Size(529, 205);
      this.config_rtf.TabIndex = 7;
      this.config_rtf.Text = "";
      this.connect_btn.Enabled = false;
      this.connect_btn.Location = new Point(441, 418);
      this.connect_btn.Name = "connect_btn";
      this.connect_btn.Size = new Size(94, 23);
      this.connect_btn.TabIndex = 8;
      this.connect_btn.Text = "Připojit";
      this.connect_btn.UseVisualStyleBackColor = true;
      this.connect_btn.Click += new EventHandler(this.button5_Click);
      this.label6.AutoSize = true;
      this.label6.BackColor = Color.Transparent;
      this.label6.Location = new Point(536, 60);
      this.label6.Name = "label6";
      this.label6.Size = new Size(16, 13);
      this.label6.TabIndex = 9;
      this.label6.Text = "->";
      this.groupBox2.BackColor = Color.Transparent;
      this.groupBox2.Controls.Add((Control) this.label7);
      this.groupBox2.ForeColor = SystemColors.HotTrack;
      this.groupBox2.Location = new Point(550, 38);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new Size(148, 100);
      this.groupBox2.TabIndex = 10;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Zkratky módů";
      this.label7.AutoSize = true;
      this.label7.ForeColor = SystemColors.Desktop;
      this.label7.Location = new Point(6, 16);
      this.label7.Name = "label7";
      this.label7.Size = new Size(121, 78);
      this.label7.TabIndex = 0;
      this.label7.Text = "dm - Free for All\r\nwar - Team Deathmatch\r\nsd - Search and Destroy\r\ndom - Domination\r\nsab - Sabotage\r\nkoth - Headquarters";
      this.groupBox3.BackColor = Color.Transparent;
      this.groupBox3.Controls.Add((Control) this.button7);
      this.groupBox3.Controls.Add((Control) this.listBox1);
      this.groupBox3.Controls.Add((Control) this.button6);
      this.groupBox3.Controls.Add((Control) this.rot_combo);
      this.groupBox3.ForeColor = SystemColors.HotTrack;
      this.groupBox3.Location = new Point(544, 144);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new Size(154, 297);
      this.groupBox3.TabIndex = 11;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Rotace map";
      this.button7.ForeColor = SystemColors.InfoText;
      this.button7.Location = new Point(4, 268);
      this.button7.Name = "button7";
      this.button7.Size = new Size(142, 23);
      this.button7.TabIndex = 20;
      this.button7.Text = "Odebrat";
      this.button7.UseVisualStyleBackColor = true;
      this.button7.Click += new EventHandler(this.button7_Click);
      this.listBox1.FormattingEnabled = true;
      this.listBox1.Location = new Point(6, 46);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new Size(142, 212);
      this.listBox1.TabIndex = 19;
      this.button6.ForeColor = SystemColors.InfoText;
      this.button6.Location = new Point(106, 17);
      this.button6.Name = "button6";
      this.button6.Size = new Size(42, 23);
      this.button6.TabIndex = 18;
      this.button6.Text = ">>>";
      this.button6.UseVisualStyleBackColor = true;
      this.button6.Click += new EventHandler(this.button6_Click);
      this.rot_combo.Enabled = false;
      this.rot_combo.FormattingEnabled = true;
      this.rot_combo.Location = new Point(6, 19);
      this.rot_combo.Name = "rot_combo";
      this.rot_combo.Size = new Size(94, 21);
      this.rot_combo.TabIndex = 17;
      this.startBezSrv_btn.Enabled = false;
      this.startBezSrv_btn.Location = new Point(6, 418);
      this.startBezSrv_btn.Name = "startBezSrv_btn";
      this.startBezSrv_btn.Size = new Size((int) sbyte.MaxValue, 23);
      this.startBezSrv_btn.TabIndex = 12;
      this.startBezSrv_btn.Text = "Spustit hru bez serveru";
      this.startBezSrv_btn.UseVisualStyleBackColor = true;
      this.startBezSrv_btn.Click += new EventHandler(this.button8_Click);
      this.linkLabel1.ActiveLinkColor = Color.Magenta;
      this.linkLabel1.AutoSize = true;
      this.linkLabel1.BackColor = Color.Transparent;
      this.linkLabel1.Font = new Font("Microsoft Sans Serif", 12f, FontStyle.Italic, GraphicsUnit.Point, (byte) 238);
      this.linkLabel1.LinkBehavior = LinkBehavior.HoverUnderline;
      this.linkLabel1.LinkColor = Color.FromArgb((int) byte.MaxValue, 0, 128);
      this.linkLabel1.Location = new Point(643, 447);
      this.linkLabel1.Name = "linkLabel1";
      this.linkLabel1.Size = new Size(55, 20);
      this.linkLabel1.TabIndex = 13;
      this.linkLabel1.TabStop = true;
      this.linkLabel1.Text = "Fílek :)";
      this.linkLabel1.VisitedLinkColor = SystemColors.HotTrack;
      this.linkLabel1.LinkClicked += new LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
      this.rcon_btn.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 238);
      this.rcon_btn.ForeColor = SystemColors.HotTrack;
      this.rcon_btn.Location = new Point(6, 444);
      this.rcon_btn.Name = "rcon_btn";
      this.rcon_btn.Size = new Size(264, 23);
      this.rcon_btn.TabIndex = 14;
      this.rcon_btn.Text = "RCON Control panel";
      this.rcon_btn.UseVisualStyleBackColor = true;
      this.rcon_btn.Click += new EventHandler(this.rcon_btn_Click);
      this.button2.Location = new Point(550, 12);
      this.button2.Name = "button2";
      this.button2.Size = new Size(148, 23);
      this.button2.TabIndex = 15;
      this.button2.Text = "Uložit nastavení";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click_1);
      this.button5.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 238);
      this.button5.ForeColor = SystemColors.HotTrack;
      this.button5.Location = new Point(271, 444);
      this.button5.Name = "button5";
      this.button5.Size = new Size(264, 23);
      this.button5.TabIndex = 16;
      this.button5.Text = "RCON Control panel pro jiný server";
      this.button5.UseVisualStyleBackColor = true;
      this.button5.Click += new EventHandler(this.button5_Click_1);
      this.linkLabel2.AutoSize = true;
      this.linkLabel2.BackColor = Color.Transparent;
      this.linkLabel2.ForeColor = SystemColors.Info;
      this.linkLabel2.LinkColor = SystemColors.HotTrack;
      this.linkLabel2.Location = new Point(545, 452);
      this.linkLabel2.Name = "linkLabel2";
      this.linkLabel2.Size = new Size(65, 13);
      this.linkLabel2.TabIndex = 18;
      this.linkLabel2.TabStop = true;
      this.linkLabel2.Text = "Nová verze!";
      this.linkLabel2.Visible = false;
      this.linkLabel2.LinkClicked += new LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
      this.label9.AutoSize = true;
      this.label9.BackColor = Color.Transparent;
      this.label9.ForeColor = SystemColors.Desktop;
      this.label9.Location = new Point(545, 452);
      this.label9.Name = "label9";
      this.label9.Size = new Size(37, 13);
      this.label9.TabIndex = 19;
      this.label9.Text = "Verze:";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = SystemColors.Control;
      this.ClientSize = new Size(704, 474);
      this.Controls.Add((Control) this.label9);
      this.Controls.Add((Control) this.linkLabel2);
      this.Controls.Add((Control) this.button5);
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.rcon_btn);
      this.Controls.Add((Control) this.linkLabel1);
      this.Controls.Add((Control) this.startBezSrv_btn);
      this.Controls.Add((Control) this.groupBox3);
      this.Controls.Add((Control) this.groupBox2);
      this.Controls.Add((Control) this.label6);
      this.Controls.Add((Control) this.connect_btn);
      this.Controls.Add((Control) this.config_rtf);
      this.Controls.Add((Control) this.start_btn);
      this.Controls.Add((Control) this.ip_text);
      this.Controls.Add((Control) this.ip_lbl);
      this.Controls.Add((Control) this.groupBox1);
      this.Controls.Add((Control) this.button1);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.iw3mp_txt);
      this.ForeColor = SystemColors.InfoText;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximumSize = new Size(720, 512);
      this.MinimumSize = new Size(720, 512);
      this.Name = nameof (Form1);
      this.Text = "Call of Duty: MW Server tools";
      this.Load += new EventHandler(this.Form1_Load);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public Form1() => this.InitializeComponent();

    private void button1_Click(object sender, EventArgs e) => this.nacist();

    private void Form1_Load(object sender, EventArgs e)
    {
      try
      {
        if (!System.IO.File.Exists(Application.StartupPath + "/updater.exe"))
          new WebClient().DownloadFile("http://filek.4fan.cz/mws/updater.exe", "updater.exe");
        string end = new StreamReader(Application.StartupPath + "/version.txt").ReadToEnd();
        this.label9.Text = "Verze: " + end;
        string str = new WebClient().DownloadString("http://filek.4fan.cz/mws/version.txt");
        int int32_1 = Convert.ToInt32(end);
        int int32_2 = Convert.ToInt32(str);
        if (int32_2 > int32_1)
        {
          if (MessageBox.Show("Je k dispozici nová verze! Chcete jí nyní stáhnout?", "Info", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk) == DialogResult.OK)
          {
            Process.Start(Application.StartupPath + "/updater.exe");
            Application.Exit();
          }
          else
          {
            this.label9.Visible = false;
            this.linkLabel2.Visible = true;
          }
        }
        else if (int32_1 > int32_2)
        {
          int num = (int) MessageBox.Show("Neplatná verze!", "Chyba!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
          Process.Start("updater.exe");
          Application.Exit();
        }
        if (System.IO.File.Exists(Application.StartupPath + "/settings.ini"))
          this.nacistZIni();
        this.ip_text.Text = Dns.Resolve(Dns.GetHostName()).AddressList[0].ToString();
        Program.ip = this.ip_text.Text;
        Program.port = 28960;
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Chyba: " + ex.Message, "Chyba!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      Process.Start(Application.StartupPath + "/updater.exe");
      Application.Exit();
    }

    private void button5_Click_1(object sender, EventArgs e) => new RCONfrmOth().Show();

    private void checkBox1_CheckedChanged(object sender, EventArgs e)
    {
      try
      {
        if (this.checkBox1.Checked)
        {
          OpenFileDialog openFileDialog = new OpenFileDialog();
          openFileDialog.Filter = "Config|*cfg";
          openFileDialog.Title = "Vyberte config pro server";
          if (openFileDialog.ShowDialog() != DialogResult.OK)
            return;
          this.config = openFileDialog.FileName;
          this.cfg_text.Text = Path.GetFileName(openFileDialog.FileName);
          StreamReader streamReader = new StreamReader(this.config);
          this.config_rtf.Text = streamReader.ReadToEnd();
          streamReader.Close();
        }
        else
        {
          this.config_rtf.Text = "";
          this.cfg_text.Text = "";
        }
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Chyba: " + ex.Message, "Chyba!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    private void button2_Click(object sender, EventArgs e)
    {
      string text = this.server_txt.Text;
      try
      {
        string str1 = "";
        string str2 = this.listBox1.SelectedItem.ToString();
        for (int index = 1; index < this.listBox1.Items.Count; ++index)
        {
          this.listBox1.SelectedIndex = index;
          str1 = str1 + "map " + this.listBox1.SelectedItem.ToString() + " ";
        }
        this.listBox1.SelectedIndex = 0;
        if (this.checkBox1.Checked && this.checkBox2.Checked)
        {
          string str3 = this.iw3mp_txt.Text + "\" +set fs_game \"mods\\" + this.modif_combo.SelectedItem + "\" +g_gametype " + this.game_text.Text + " +map " + str2 + " +set dedicated " + this.dedik_combo.SelectedIndex.ToString() + " +exec " + Path.GetFileName(this.cfg_text.Text) + " +sv_maprotation " + str1 + " +rcon_password " + Program.rcon + " +sv_hostname " + text;
          new Process()
          {
            StartInfo = {
              FileName = this.iw3mp_txt.Text,
              WorkingDirectory = Path.GetDirectoryName(this.iw3mp_txt.Text),
              Arguments = str3
            }
          }.Start();
        }
        else if (this.checkBox1.Checked && !this.checkBox2.Checked)
        {
          string str3 = this.iw3mp_txt.Text + "\" +g_gametype " + this.game_text.Text + " +set dedicated " + this.dedik_combo.SelectedIndex.ToString() + " +map " + str2 + " +exec " + Path.GetFileName(this.cfg_text.Text) + " +sv_maprotation " + str1 + " +sv_hostname " + text + " +rcon_password " + Program.rcon;
          new Process()
          {
            StartInfo = {
              FileName = this.iw3mp_txt.Text,
              WorkingDirectory = Path.GetDirectoryName(this.iw3mp_txt.Text),
              Arguments = str3
            }
          }.Start();
        }
        else if (!this.checkBox1.Checked && this.checkBox2.Checked)
        {
          string str3 = this.iw3mp_txt.Text + "\" +set fs_game \"mods\\" + this.modif_combo.SelectedItem + "\" +g_gametype " + this.game_text.Text + " +map " + str2 + " +set dedicated " + this.dedik_combo.SelectedIndex.ToString() + " +sv_maprotation " + str1 + " +sv_hostname " + text + " +rcon_password " + Program.rcon;
          new Process()
          {
            StartInfo = {
              FileName = this.iw3mp_txt.Text,
              WorkingDirectory = Path.GetDirectoryName(this.iw3mp_txt.Text),
              Arguments = str3
            }
          }.Start();
        }
        else if (!this.checkBox1.Checked && !this.checkBox2.Checked)
        {
          string str3 = this.iw3mp_txt.Text + "\" +g_gametype " + this.game_text.Text + " +map " + str2 + " +set dedicated " + this.dedik_combo.SelectedIndex.ToString() + " +sv_maprotation " + str1 + " +sv_hostname " + text + " +rcon_password " + Program.rcon;
          new Process()
          {
            StartInfo = {
              FileName = this.iw3mp_txt.Text,
              WorkingDirectory = Path.GetDirectoryName(this.iw3mp_txt.Text),
              Arguments = str3
            }
          }.Start();
        }
        this.connect_btn.Enabled = true;
        this.rcon_btn.Enabled = true;
        this.rcon_txt.Enabled = false;
        this.server_txt.Enabled = false;
        this.funkce.bublinaZobrazit("Server", "Server byl úspěšně vytvořen s IP " + this.ip_text.Text, this.Icon, 10000);
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Chyba: " + ex.Message, "Chyba!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    private void button3_Click(object sender, EventArgs e)
    {
      try
      {
        StreamWriter streamWriter = new StreamWriter(this.config);
        for (int index = 0; index < this.config_rtf.Lines.Length; ++index)
          streamWriter.WriteLine(this.config_rtf.Lines[index]);
        streamWriter.Close();
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Chyba: " + ex.Message + " Nejdříve musíte vytvořit nový CFG nebo otevřít již vytvořený", "Chyba!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    private void button4_Click(object sender, EventArgs e)
    {
      SaveFileDialog saveFileDialog = new SaveFileDialog();
      saveFileDialog.Filter = "Config|*.cfg";
      saveFileDialog.Title = "Uložit config";
      if (saveFileDialog.ShowDialog() != DialogResult.OK)
        return;
      this.config = saveFileDialog.FileName;
      StreamWriter streamWriter = new StreamWriter(this.config);
      streamWriter.Flush();
      streamWriter.Close();
      this.cfg_text.Text = Path.GetFileName(this.config);
    }

    private void checkBox2_CheckedChanged(object sender, EventArgs e)
    {
      if (this.checkBox2.Checked)
        this.modif_combo.Enabled = true;
      else
        this.modif_combo.Enabled = false;
    }

    private void button5_Click(object sender, EventArgs e)
    {
      try
      {
        string str = "\" +connect " + this.ip_text.Text;
        new Process()
        {
          StartInfo = {
            FileName = this.iw3mp_txt.Text,
            WorkingDirectory = Path.GetDirectoryName(this.iw3mp_txt.Text),
            Arguments = str
          }
        }.Start();
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Chyba: " + ex.Message, "Chyba!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    private void button6_Click(object sender, EventArgs e)
    {
      try
      {
        this.listBox1.Items.Add(this.rot_combo.SelectedItem);
        this.listBox1.SelectedIndex = 0;
      }
      catch
      {
        int num = (int) MessageBox.Show("Musíte vybrat mapu ze seznamu", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
      }
    }

    private void button7_Click(object sender, EventArgs e)
    {
      try
      {
        this.listBox1.Items.Remove(this.listBox1.SelectedItem);
        this.listBox1.SelectedIndex = 0;
      }
      catch
      {
        int num = (int) MessageBox.Show("Musíte vybrat mapu ze seznamu", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
      }
    }

    private void button8_Click(object sender, EventArgs e)
    {
      try
      {
        new Process()
        {
          StartInfo = {
            FileName = this.iw3mp_txt.Text,
            WorkingDirectory = Path.GetDirectoryName(this.iw3mp_txt.Text)
          }
        }.Start();
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Chyba: " + ex.Message, "Chyba!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      int num = (int) MessageBox.Show("Made by Fílek 2011\n\n       ..::THX to::..\nKyry - RegEX\nŠéďa, Lukadoss - Testers", "Info", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
    }

    private void rcon_btn_Click(object sender, EventArgs e) => new RCONfrm().Show();

    private void rcon_txt_TextChanged(object sender, EventArgs e) => Program.rcon = this.rcon_txt.Text;

    private void button2_Click_1(object sender, EventArgs e)
    {
      Ini ini = new Ini(Application.StartupPath + "/settings.ini");
      if (this.iw3mp_txt.Text != string.Empty)
      {
        ini.Zapsat("Settings", "Path", this.iw3mp_txt.Text);
        ini.Zapsat("Settings", "Mod", this.game_text.Text);
        ini.Zapsat("Settings", "Hostname", this.server_txt.Text);
      }
      else
      {
        int num = (int) MessageBox.Show("Nejdříve musíte nastavit cestu k iw3mp.exe", "Chyba!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    public void nacist()
    {
      try
      {
        OpenFileDialog openFileDialog = new OpenFileDialog();
        openFileDialog.Filter = "iw3mp.exe|iw3mp.exe";
        openFileDialog.InitialDirectory = "C:\\";
        openFileDialog.Title = "Vyberte soubor iw3mp.exe";
        if (openFileDialog.ShowDialog() == DialogResult.OK)
          this.iw3mp_txt.Text = openFileDialog.FileName;
        this.modifikace = Path.GetDirectoryName(this.iw3mp_txt.Text);
        this.modifikace += "\\Mods";
        this.startBezSrv_btn.Enabled = true;
        this.start_btn.Enabled = true;
        foreach (FileSystemInfo directory in new DirectoryInfo(this.modifikace).GetDirectories("*", SearchOption.TopDirectoryOnly))
          this.modif_combo.Items.Add((object) directory.Name);
        this.modif_combo.SelectedIndex = 0;
        foreach (object obj in this.mapy)
          this.rot_combo.Items.Add(obj);
        this.dedik_combo.SelectedIndex = 0;
        this.rot_combo.SelectedIndex = 0;
        this.rot_combo.Enabled = true;
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Chyba: " + ex.Message, "Chyba!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    public void nacistZIni()
    {
      Ini ini = new Ini(Application.StartupPath + "/settings.ini");
      this.iw3mp_txt.Text = ini.Precist("Settings", "Path");
      this.game_text.Text = ini.Precist("Settings", "Mod");
      this.server_txt.Text = ini.Precist("Settings", "Hostname");
      this.modifikace = Path.GetDirectoryName(this.iw3mp_txt.Text);
      this.modifikace += "\\Mods";
      this.startBezSrv_btn.Enabled = true;
      this.start_btn.Enabled = true;
      foreach (FileSystemInfo directory in new DirectoryInfo(this.modifikace).GetDirectories("*", SearchOption.TopDirectoryOnly))
        this.modif_combo.Items.Add((object) directory.Name);
      this.modif_combo.SelectedIndex = 0;
      foreach (object obj in this.mapy)
        this.rot_combo.Items.Add(obj);
      this.dedik_combo.SelectedIndex = 0;
      this.rot_combo.SelectedIndex = 0;
      this.rot_combo.Enabled = true;
    }

    private void iw3mp_txt_TextChanged(object sender, EventArgs e) => Program.cesta = this.iw3mp_txt.Text;
  }
}
