﻿// Decompiled with JetBrains decompiler
// Type: MWS.RCON
// Assembly: MWS, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BDEF8AEC-8A94-4FB3-8152-B098A3F52774
// Assembly location: C:\Users\Fifal\Desktop\MWS.exe

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace MWS
{
  internal class RCON
  {
    public string sendCommand(
      string rconCommand,
      string gameServerIP,
      string password,
      int gameServerPort)
    {
      string str;
      try
      {
        Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        socket.ReceiveTimeout = 2000;
        socket.Connect(IPAddress.Parse(gameServerIP), gameServerPort);
        byte[] bytes = Encoding.ASCII.GetBytes("rcon " + password + " " + rconCommand);
        byte[] buffer = new byte[bytes.Length + 5 - 1 + 1];
        buffer[0] = byte.Parse("255");
        buffer[1] = byte.Parse("255");
        buffer[2] = byte.Parse("255");
        buffer[3] = byte.Parse("255");
        buffer[4] = byte.Parse("02");
        int index1 = 5;
        int num = bytes.Length - 1;
        for (int index2 = 0; index2 <= num; ++index2)
        {
          buffer[index1] = bytes[index2];
          ++index1;
        }
        IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Any, 0);
        socket.Send(buffer, SocketFlags.None);
        byte[] numArray = new byte[65000];
        socket.Receive(numArray);
        Encoding.ASCII.GetString(numArray).Contains("Invalid password");
        str = Encoding.ASCII.GetString(numArray);
      }
      catch (Exception ex)
      {
        str = "";
        int num = (int) MessageBox.Show("Chyba: " + ex.Message, "Chyba!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      return str;
    }
  }
}
