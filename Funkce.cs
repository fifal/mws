﻿// Decompiled with JetBrains decompiler
// Type: MWS.Funkce
// Assembly: MWS, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BDEF8AEC-8A94-4FB3-8152-B098A3F52774
// Assembly location: C:\Users\Fifal\Desktop\MWS.exe

using System;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace MWS
{
  internal class Funkce
  {
    public void bublinaZobrazit(string nadpis, string text, Icon icon, int doba) => new NotifyIcon()
    {
      BalloonTipTitle = nadpis,
      BalloonTipText = text,
      Icon = icon,
      Visible = true,
      BalloonTipIcon = ToolTipIcon.Info
    }.ShowBalloonTip(doba);

    public string odeslatPrikaz(string prikaz)
    {
      try
      {
        string rcon = Program.rcon;
        string ip = Program.ip;
        int port = Program.port;
        return new RCON().sendCommand(prikaz, ip, rcon, port);
      }
      catch (Exception ex)
      {
        return "Chyba: " + ex.Message;
      }
    }

    public void nacistSeznamHracu()
    {
      try
      {
        Program.szn.seznamGrid.Rows.Clear();
        foreach (Match match in new Regex("(?<id>[0-9]+)\\s+(?<score>[0-9-]+)\\s+(?<ping>[0-9]+)\\s+(?<guid>[a-z0-9]+)\\s+(?<name>.*?)\\s+(?<last>[0-9]+)\\s+(?<ip>[0-9.]+):(?<port>[0-9-]+)\\s+(?<qport>[0-9-]+)\\s+(?<rate>[0-9]+)", RegexOptions.IgnoreCase).Matches(this.odeslatPrikaz("status")))
        {
          Program.szn.seznamGrid.BackgroundColor = Color.DarkGray;
          string str = new Regex("\\^\\d").Replace(match.Groups["name"].Value, "");
          Program.szn.seznamGrid.Rows.Add((object) match.Groups["id"].Value, (object) match.Groups["ping"].Value, (object) match.Groups["guid"].Value, (object) str, (object) match.Groups["score"].Value);
        }
        for (int index = 0; index < Program.szn.seznamGrid.Rows.Count; ++index)
        {
          if (index % 2 == 1)
            Program.szn.seznamGrid.Rows[index].DefaultCellStyle.BackColor = Color.LightGray;
        }
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Chyba: " + ex.Message, "Chyba!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    public void nacistSeznamBanu()
    {
      Program.ban.dataGridView1.Rows.Clear();
      StreamReader streamReader = new StreamReader(Path.GetDirectoryName(Program.cesta) + "/main/ban.txt");
      string end = streamReader.ReadToEnd();
      streamReader.Close();
      foreach (Match match in new Regex("(?<guid>[A-Za-z0-9]+)\\s(?<nick>.*)").Matches(end))
      {
        string str = match.Groups["nick"].Value.Replace("\r\r", "");
        Program.ban.dataGridView1.Rows.Add((object) str, (object) match.Groups["guid"].Value);
      }
      for (int index = 0; index < Program.ban.dataGridView1.Rows.Count; ++index)
      {
        if (index % 2 == 1)
          Program.ban.dataGridView1.Rows[index].DefaultCellStyle.BackColor = Color.LightGray;
      }
    }
  }
}
