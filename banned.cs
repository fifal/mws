﻿// Decompiled with JetBrains decompiler
// Type: MWS.banned
// Assembly: MWS, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BDEF8AEC-8A94-4FB3-8152-B098A3F52774
// Assembly location: C:\Users\Fifal\Desktop\MWS.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace MWS
{
  public class banned : Form
  {
    private IContainer components;
    private DataGridViewTextBoxColumn nick;
    private DataGridViewTextBoxColumn GUID;
    private Button button1;
    private Button button2;
    public DataGridView dataGridView1;
    private Funkce fce = new Funkce();

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (banned));
      this.dataGridView1 = new DataGridView();
      this.nick = new DataGridViewTextBoxColumn();
      this.GUID = new DataGridViewTextBoxColumn();
      this.button1 = new Button();
      this.button2 = new Button();
      ((ISupportInitialize) this.dataGridView1).BeginInit();
      this.SuspendLayout();
      this.dataGridView1.AllowUserToAddRows = false;
      this.dataGridView1.AllowUserToDeleteRows = false;
      this.dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Columns.AddRange((DataGridViewColumn) this.nick, (DataGridViewColumn) this.GUID);
      this.dataGridView1.Location = new Point(12, 12);
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.ReadOnly = true;
      this.dataGridView1.Size = new Size(513, 208);
      this.dataGridView1.TabIndex = 0;
      this.dataGridView1.CellDoubleClick += new DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
      this.nick.HeaderText = "Nick";
      this.nick.Name = "nick";
      this.nick.ReadOnly = true;
      this.nick.Width = 215;
      this.GUID.HeaderText = "GUID";
      this.GUID.Name = "GUID";
      this.GUID.ReadOnly = true;
      this.GUID.Width = (int) byte.MaxValue;
      this.button1.Location = new Point(12, 224);
      this.button1.Name = "button1";
      this.button1.Size = new Size(320, 20);
      this.button1.TabIndex = 1;
      this.button1.Text = "Refresh";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.button2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 238);
      this.button2.ForeColor = Color.Red;
      this.button2.Location = new Point(338, 224);
      this.button2.Name = "button2";
      this.button2.Size = new Size(188, 20);
      this.button2.TabIndex = 2;
      this.button2.Text = "Konec";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(538, 248);
      this.ControlBox = false;
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.button1);
      this.Controls.Add((Control) this.dataGridView1);
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximumSize = new Size(554, 286);
      this.MinimumSize = new Size(554, 286);
      this.Name = nameof (banned);
      this.Text = "Zabanovaní hráči";
      this.Load += new EventHandler(this.banned_Load);
      ((ISupportInitialize) this.dataGridView1).EndInit();
      this.ResumeLayout(false);
    }

    public banned() => this.InitializeComponent();

    private void banned_Load(object sender, EventArgs e) => this.nacti();

    public void nacti() => this.fce.nacistSeznamBanu();

    private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
    {
      Program.nick = this.dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
      Program.guid = this.dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
      if (MessageBox.Show("Opravdu chcete odbanovat uživatele \"" + Program.nick + "\" ?", "To snad nemyslíš vážně!", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk) != DialogResult.OK)
        return;
      StreamReader streamReader = new StreamReader(Path.GetDirectoryName(Program.cesta) + "/main/ban.txt");
      string str = streamReader.ReadToEnd().Replace(Program.guid + " " + Program.nick, "");
      streamReader.Close();
      StreamWriter streamWriter = new StreamWriter(Path.GetDirectoryName(Program.cesta) + "/main/ban.txt");
      streamWriter.Write(str);
      streamWriter.Close();
      this.dataGridView1.Rows.Clear();
      this.nacti();
    }

    private void button1_Click(object sender, EventArgs e) => this.nacti();

    private void button2_Click(object sender, EventArgs e) => Program.ban.Hide();
  }
}
