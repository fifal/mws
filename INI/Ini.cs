﻿// Decompiled with JetBrains decompiler
// Type: INI.Ini
// Assembly: MWS, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BDEF8AEC-8A94-4FB3-8152-B098A3F52774
// Assembly location: C:\Users\Fifal\Desktop\MWS.exe

using System.Runtime.InteropServices;
using System.Text;

namespace INI
{
  public class Ini
  {
    public string path;

    public Ini(string INIPath) => this.path = INIPath;

    [DllImport("kernel32")]
    private static extern int GetPrivateProfileString(
      string section,
      string key,
      string def,
      StringBuilder retVal,
      int size,
      string filePath);

    public string Precist(string Section, string Key)
    {
      StringBuilder retVal = new StringBuilder((int) byte.MaxValue);
      Ini.GetPrivateProfileString(Section, Key, "", retVal, (int) byte.MaxValue, this.path);
      return retVal.ToString();
    }

    [DllImport("kernel32")]
    private static extern long WritePrivateProfileString(
      string section,
      string key,
      string val,
      string filePath);

    public void Zapsat(string Section, string Key, string Value) => Ini.WritePrivateProfileString(Section, Key, Value, this.path);
  }
}
