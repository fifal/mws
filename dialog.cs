﻿// Decompiled with JetBrains decompiler
// Type: MWS.dialog
// Assembly: MWS, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BDEF8AEC-8A94-4FB3-8152-B098A3F52774
// Assembly location: C:\Users\Fifal\Desktop\MWS.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace MWS
{
  public class dialog : Form
  {
    private IContainer components;
    private Label nick_txt;
    private Button ban_btn;
    private Button kick_btn;
    private Button konec_btn;
    private TextBox textBox1;
    private Button poslat_btn;
    private Funkce fce = new Funkce();

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (dialog));
      this.nick_txt = new Label();
      this.ban_btn = new Button();
      this.kick_btn = new Button();
      this.konec_btn = new Button();
      this.textBox1 = new TextBox();
      this.poslat_btn = new Button();
      this.SuspendLayout();
      this.nick_txt.AutoSize = true;
      this.nick_txt.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Italic, GraphicsUnit.Point, (byte) 238);
      this.nick_txt.Location = new Point(12, 9);
      this.nick_txt.Name = "nick_txt";
      this.nick_txt.Size = new Size(35, 13);
      this.nick_txt.TabIndex = 0;
      this.nick_txt.Text = "label1";
      this.ban_btn.Location = new Point(12, 61);
      this.ban_btn.Name = "ban_btn";
      this.ban_btn.Size = new Size(124, 23);
      this.ban_btn.TabIndex = 1;
      this.ban_btn.Text = "BAN";
      this.ban_btn.UseVisualStyleBackColor = true;
      this.ban_btn.Click += new EventHandler(this.ban_btn_Click);
      this.kick_btn.Location = new Point(142, 61);
      this.kick_btn.Name = "kick_btn";
      this.kick_btn.Size = new Size(124, 23);
      this.kick_btn.TabIndex = 2;
      this.kick_btn.Text = "Kick";
      this.kick_btn.UseVisualStyleBackColor = true;
      this.kick_btn.Click += new EventHandler(this.kick_btn_Click);
      this.konec_btn.Location = new Point(272, 61);
      this.konec_btn.Name = "konec_btn";
      this.konec_btn.Size = new Size(124, 23);
      this.konec_btn.TabIndex = 3;
      this.konec_btn.Text = "Konec";
      this.konec_btn.UseVisualStyleBackColor = true;
      this.konec_btn.Click += new EventHandler(this.konec_btn_Click);
      this.textBox1.Location = new Point(12, 35);
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new Size(254, 20);
      this.textBox1.TabIndex = 4;
      this.poslat_btn.Location = new Point(273, 33);
      this.poslat_btn.Name = "poslat_btn";
      this.poslat_btn.Size = new Size(124, 23);
      this.poslat_btn.TabIndex = 5;
      this.poslat_btn.Text = "Poslat zprávu";
      this.poslat_btn.UseVisualStyleBackColor = true;
      this.poslat_btn.Click += new EventHandler(this.poslat_btn_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(409, 96);
      this.Controls.Add((Control) this.poslat_btn);
      this.Controls.Add((Control) this.textBox1);
      this.Controls.Add((Control) this.konec_btn);
      this.Controls.Add((Control) this.kick_btn);
      this.Controls.Add((Control) this.ban_btn);
      this.Controls.Add((Control) this.nick_txt);
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.Name = nameof (dialog);
      this.Text = "Info";
      this.Load += new EventHandler(this.dialog_Load);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public dialog() => this.InitializeComponent();

    private void dialog_Load(object sender, EventArgs e) => this.nick_txt.Text = "Hráč \"" + Program.nick + "\" s id " + (object) Program.id + "; Skóre: " + Program.score;

    private void kick_btn_Click(object sender, EventArgs e) => this.fce.odeslatPrikaz("clientkick " + (object) Program.id);

    private void konec_btn_Click(object sender, EventArgs e) => this.Close();

    private void ban_btn_Click(object sender, EventArgs e) => this.fce.odeslatPrikaz("banClient " + (object) Program.id);

    private void poslat_btn_Click(object sender, EventArgs e)
    {
      string text = this.textBox1.Text;
      this.fce.odeslatPrikaz("tell " + (object) Program.id + " " + text);
    }
  }
}
